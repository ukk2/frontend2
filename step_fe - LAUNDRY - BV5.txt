### Step By Step React-laundry

## S-By-S FIRST SETTING
- npm install global react (jika perlu) -> $ npm install -g create-react-app

- pastikan keluar dari folder

- npm install react buat project -> $ npx create-react-app frontend

- masuk ke dalam folder "frontend"

- untuk menjalankan program ketik -> $ npm start 

- Instal library React Router DOM -> $ npm install react-router-dom

- buat index.js mendukung react-router -> import {BrowserRouter} from "react-router-dom";
  lalu ganti tag pada luar <App/>

- buat App.js -> jadi tempat link route
```
//example link route

import React from "react"
import { Switch, Route } from "react-router-dom";
import Login from "./pages/Login"
import Product from "./pages/Product"
import Customer from "./pages/Customer"
import Transaction from "./pages/Transaction"
import Home from "./pages/Home"
import Admin from "./pages/Admin"

export default class App extends React.Component{
  render(){
    return(
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/home" component={Home} />
        <Route path="/product" component={Product} />
        <Route path="/customer" component={Customer} />
        <Route path="/transaction" component={Transaction} />
        <Route path="/admin" component={Admin} />
      </Switch>
    )
  }
}
```

- install react-icons -> npm install react-icons --save 

### komponen penting React-router
- instalasi react-router-dom
- import -> import { BrowserRouter as Router, Switch, Route } from "react-router-dom"; //ini ga perlu sih
- dekatkan Navbar + Router 
     -> <Router>
          <Navbar />
          <Switch>
            {/* <Route exact path="/" component={Login} /> */}
            <Route path="/siswa" component={Siswa} />
          </Switch>
        </Router>
- import Link di Navbar.jsx
     -> import { Link } from "react-router-dom";

## S-By-S FLOW BUAT PROJECTNYA

- npm install react buat project -> $ npx create-react-app frontend

- masuk ke dalam folder "frontend"

- untuk menjalankan program ketik utk cek berhasil pa nggak nya -> $ npm start 

- Instal library React Router DOM -> $ npm install react-router-dom

- install axios dkk -> npm install axios --save

- install react-icons -> npm install react-icons --save

- install boostrap -> npm install bootstrap --save

- instal popper -> npm install jquery popper.js (-sek gatau gunane)

- install crypto.js -> npm install crypto-js

- install moment -> npm install moment

- buat file config.js di luar folder semuanya -> isinya
export const baseUrl = "http://localhost:8080/admin";

- buat folder assets,components,pages,utils (details nya ada di https://github.com/nandaha29/react-boostrap-sidebar.git)



- susun halaman
menggunakan class komponent

- susun script

## S-By-S FLOW BUAT REACT-ROUTER

- setting isi App.js
`````````` start code
import React from "react";
// import { Switch, Route } from "react-router-dom";
import { Routes, Route } from "react-router-dom";

//call pagses
import Home from "./pages/Home";
import Paket from "./pages/Paket";
import Karyawan from "./pages/User";
import Outlet from "./pages/Outlet";
import Member from "./pages/Member";
import Transaksi from "./pages/Transaksi";
import User from "./pages/User";
import Login from "./pages/Login";
import "./App.css";

export default function App() {
  return (
    <Routes>
      <Route path="/login" element={<Login />} />
      <Route path="/" element={<Home />} />
      <Route path="/home" element={<Home />} />
      <Route path="/paket" element={<Paket />} />
      <Route path="/karyawan" element={<Karyawan />} />
      <Route path="/outlet" element={<Outlet />} />
      <Route path="/member" element={<Member />} />
      <Route path="/transaksi" element={<Transaksi />} />
      <Route path="/User" element={<User />} />
    </Routes>
  );
}

`````````` end code

- edit index.js
`````````` start code
//Abaikan aja disini, cuman tempat import buat global, mirip app.js di node

import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
// call elemen
import { BrowserRouter } from "react-router-dom";

// Import Boostrap
import "bootstrap/dist/css/bootstrap.min.css";
// import $ from 'jquery';
// import Popper from 'popper.js';
import "bootstrap/dist/js/bootstrap.bundle.min";

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

`````````` end code



# NB: susunan Node versi pak Jack:D
- react-router-dom =
- axios =
- popper.js =

AYOK BISA YOK:)
